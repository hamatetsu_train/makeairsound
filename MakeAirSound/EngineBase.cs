﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Text;
//using System.Threading.Tasks;
using System.Windows.Forms;

namespace AtsPlugin
{
    public class EngineBase
    {
        #region static

        #region - DLLのパス
        private static string pModulePathWithFileName = "";
        public static string ModulePathWithFileName
        {
            get
            {
                if (pModulePathWithFileName == "")
                {
                    pModulePathWithFileName = Assembly.GetExecutingAssembly().Location;
                }
                return pModulePathWithFileName;
            }
        }
        #endregion

        #region - DLLのファイル名
        private static string pModuleName = "";
        public static string ModuleName
        {
            get
            {
                if (pModuleName == "")
                {
                    pModuleName = Path.GetFileName(ModulePathWithFileName);
                }
                return pModuleName;
            }
        }
        #endregion

        #region - DLLのフォルダ
        private static string pModuleDirectoryPath = "";
        public static string ModuleDirectoryPath
        {
            get
            {
                if (pModuleDirectoryPath == "")
                {
                    pModuleDirectoryPath = Path.GetDirectoryName(ModulePathWithFileName); ;
                }
                return pModuleDirectoryPath;
            }
        }
        #endregion

        #region "- ログ"
        public static class Log
        {
            private static string FilePath = "";
            private static FileStream fs = null;
            private static bool fserror = false;

            public static void Init()
            {
                try
                {
                    // 使われてたらそのまま使う
                    if (fs == null)
                    {
                        // ログファイルパスの生成
                        FilePath = ModulePathWithFileName.Remove(ModulePathWithFileName.Length - 4, 4) + ".log";
                        // ファイルを空にしたい
                        if (File.Exists(FilePath) == true)
                        {
                            File.Delete(FilePath);
                        }
                        // オープン
                        fs = File.Open(FilePath, FileMode.OpenOrCreate);
                        fs.Seek(0, SeekOrigin.End);
                    }

                }
                catch (Exception ex)
                {
                    MessageBox.Show("ログファイルオープン処理でエラーが発生しました。\n" + ex.Message, "ATS-PTプラグイン");
                }
            }
            public static void Dispose()
            {
                try
                {
                    if (fs != null)
                    {
                        fs.Dispose();
                        //fs = null;
                    }
                }
                catch (Exception ex)
                {
                    MessageBox.Show("ログファイルクローズ処理でエラーが発生しました。\n" + ex.Message, "ATS-PTプラグイン");
                }
            }

            public static void Write(string message)
            {
                try
                {
                    if (fs != null)
                    {
                        UTF8Encoding utf8 = new UTF8Encoding();
                        byte[] result = utf8.GetBytes(DateTime.Now.ToString("HH:mm:ss.fff") + ":" + message + "\n");
                        //fs.WriteAsync(result, 0, result.Length);
                    }
                }
                catch
                {
                    try
                    {
                        if (fs != null && fserror == false)
                        {
                            // 一度生成を試す
                            // ログファイルパスの生成
                            FilePath = ModulePathWithFileName.Remove(ModulePathWithFileName.Length - 4, 4) + ".log";
                            // オープン
                            fs = File.Open(FilePath, FileMode.OpenOrCreate);
                            fs.Seek(0, SeekOrigin.End);
                        }
                    }
                    catch (Exception ex2)
                    {
                        MessageBox.Show("ログ処理でエラーが発生しました。\n" + ex2.Message, "ATS-PTプラグイン");
                        fserror = true;
                    }
                }
            }
        }

        #endregion

        #endregion

        #region エラー表示
        /// <summary>
        /// エラー表示回数
        /// </summary>
        private int ExceptionCounter = 0;

        /// <summary>
        /// １回目だけエラーダイアログを表示する
        /// </summary>
        /// <param name="ex"></param>
        /// <param name="addtext"></param>
        /// <param name="showDialog"></param>
        public void ExceptionMessage(Exception ex, string addtext, bool showDialog)
        {
#if DEBUG
            if (ExceptionCounter == 0){
                ExceptionCounter++;
                MessageBox.Show(addtext + "\n" + ex.Message, "ATS-PTプラグイン");
            }
            else
            {
                Console.Write(addtext + "\n" + ex.Message);
            }
#else
            if (showDialog == true && ExceptionCounter == 0)
            {
                ExceptionCounter++;
                MessageBox.Show(addtext + "\n" + ex.Message, "ATS-PTプラグイン");
            }
            else
            {
                Console.Write(addtext + "\n" + ex.Message);
            }
            EngineBase.Log.Write(addtext + " (Error) " + ex.Message);
#endif
        }
        #endregion
    }
}
