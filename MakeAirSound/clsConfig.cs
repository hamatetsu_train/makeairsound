﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
//using System.Threading.Tasks;
using System.Xml.Linq;

namespace AtsPlugin
{
    public class clsConfig : EngineBase
    {
        #region メンバ定義
        public bool DebugMode;

        public string MotorNoiseTxtPath;
        public string SoundTxtPath;
        public string EngineSectionName;

        /// <summary>
        /// ピストンの面積[m2]
        /// </summary>
        public double PistonArea;
        /// <summary>
        /// 緩解時シリンダー長[m]
        /// </summary>
        public double SylinderLength;
        /// <summary>
        /// バネ定数
        /// </summary>
        public double Bane;
        /// <summary>
        /// 排気管直径[m2]
        /// </summary>
        public double BlowArea;

        /// <summary>
        /// 最大変化量[mol/s]
        /// </summary>
        public double MaxA;

        public int OutReverserHandleIndex;
        public int OutPowerHandleIndex;
        public int OutBrakeHandleIndex;
        public int OutCscValueIndex;

        public int InReverserHandleIndex;
        public int InPowerHandleIndex;
        public int InBrakeHandleIndex;
        public int InCscValueIndex;

        public int InBcValueIndex;
        public int PanelBcValueIndex;
        #endregion

        #region コンストラクタ
        public clsConfig()
        {
            this.Initialize();
        }
        #endregion

        #region イニシャライズ
        public void Initialize()
        {
            try
            {
                this.DebugMode = false;

                this.MotorNoiseTxtPath = "";
                this.SoundTxtPath = "";
                this.EngineSectionName = "";

                this.PistonArea = 1;
                this.SylinderLength = 1;
                this.Bane = 1;
                this.BlowArea = 1;
                this.MaxA = 120;

                this.OutReverserHandleIndex = -1;
                this.OutPowerHandleIndex = -1;
                this.OutBrakeHandleIndex = -1;
                this.OutCscValueIndex = -1;
                this.InReverserHandleIndex = -1;
                this.InPowerHandleIndex = -1;
                this.InBrakeHandleIndex = -1;
                this.InCscValueIndex = -1;
                this.InBcValueIndex = -1;
                this.PanelBcValueIndex = -1;

            }
            catch (Exception ex)
            {
                ExceptionMessage(ex, "設定の初期化でエラーが発生しました。[Config.Load]", true);
            }
        }
        #endregion

        #region 読み込み
        public void MainLoad()
        {
            string DatPath = ModulePathWithFileName.Remove(ModulePathWithFileName.Length - 4, 4) + ".xml"; // .xmlのパスを生成

            Load(DatPath);
        }

        public void Load(string DatPath)
        {
            // 初期化
            this.Initialize();

            // 設定ファイル読み込み
            if (System.IO.File.Exists(DatPath))
            {
                try
                {
                    var row = XDocument.Load(DatPath).Element("MakeAirSound");

#if DEBUG
                    this.DebugMode = true;
#else
                    if (row.Element("Debug") != null && row.Element("Debug").Value != null)
                    {
                        if (bool.TryParse(row.Element("Debug").Value, out this.DebugMode) == false)
                        {
                            this.DebugMode = false;
                        }
                    }
#endif

                    if (row.Element("PistonArea") != null && row.Element("PistonArea").Value != null)
                    {
                        if (double.TryParse(row.Element("PistonArea").Value, out double d) == true)
                        {
                            this.PistonArea = d;
                        }
                        else
                        {
                            this.PistonArea = 1;
                        }
                    }


                    if (row.Element("SylinderLength") != null && row.Element("SylinderLength").Value != null)
                    {
                        if (double.TryParse(row.Element("SylinderLength").Value, out double d) == true)
                        {
                            this.SylinderLength = d;
                        }
                        else
                        {
                            this.SylinderLength = 1;
                        }
                    }


                    if (row.Element("Bane") != null && row.Element("Bane").Value != null)
                    {
                        if (double.TryParse(row.Element("Bane").Value, out double d) == true)
                        {
                            this.Bane = d;
                        }
                        else
                        {
                            this.Bane = 1;
                        }
                    }


                    if (row.Element("BlowArea") != null && row.Element("BlowArea").Value != null)
                    {
                        if (double.TryParse(row.Element("BlowArea").Value, out double d) == true)
                        {
                            this.BlowArea = d;
                        }
                        else
                        {
                            this.BlowArea = 1;
                        }
                    }

                    if (row.Element("MaxA") != null && row.Element("MaxA").Value != null)
                    {
                        if (double.TryParse(row.Element("MaxA").Value, out double d) == true)
                        {
                            this.MaxA = d;
                        }
                        else
                        {
                            this.MaxA = 120;
                        }
                    }


                    XElement row2 = row.Element("PowerHandleIndex");
                    if (row2 != null)
                    {
                        this.InPowerHandleIndex = GetIndexFromXml(row2, "Input", this.InPowerHandleIndex);
                        this.OutPowerHandleIndex = GetIndexFromXml(row2, "Output", this.OutPowerHandleIndex);
                        //this.PanelPowerHandleIndex = GetIndexFromXml(row2, "Panel", this.pPanelPowerHandleIndex);
                    }

                    row2 = row.Element("BrakeHandleIndex");
                    if (row2 != null)
                    {
                        this.InBrakeHandleIndex = GetIndexFromXml(row2, "Input", this.InBrakeHandleIndex);
                        this.OutBrakeHandleIndex = GetIndexFromXml(row2, "Output", this.OutBrakeHandleIndex);
                        //this.PanelBrakeHandleIndex = GetIndexFromXml(row2, "Panel", this.pPanelBrakeHandleIndex);
                    }

                    if (row.Element("MotorNoiseTxtPath") != null && row.Element("MotorNoiseTxtPath").Value != null)
                    {
                        this.MotorNoiseTxtPath = System.IO.Path.Combine(EngineBase.ModuleDirectoryPath, row.Element("MotorNoiseTxtPath").Value);
                    }

                    if (row.Element("SoundTxtPath") != null && row.Element("SoundTxtPath").Value != null)
                    {
                        this.SoundTxtPath = System.IO.Path.Combine(EngineBase.ModuleDirectoryPath, row.Element("SoundTxtPath").Value);
                    }

                    if (row.Element("EngineSectionName") != null && row.Element("EngineSectionName").Value != null)
                    {
                        this.EngineSectionName = row.Element("EngineSectionName").Value;
                    }

                    row2 = row.Element("ReverserHandleIndex");
                    if (row2 != null)
                    {
                        this.InReverserHandleIndex = GetIndexFromXml(row2, "Input", this.InReverserHandleIndex);
                        this.OutReverserHandleIndex = GetIndexFromXml(row2, "Output", this.OutReverserHandleIndex);
                        //this.PanelReverserHandleIndex = GetIndexFromXml(row2, "Panel", this.pPanelReverserHandleIndex);
                    }

                    row2 = row.Element("CscValueIndex");
                    if (row2 != null)
                    {
                        this.InCscValueIndex = GetIndexFromXml(row2, "Input", this.InCscValueIndex);
                        this.OutCscValueIndex = GetIndexFromXml(row2, "Output", this.OutCscValueIndex);
                    }

                    row2 = row.Element("BcValueIndex");
                    if (row2 != null)
                    {
                        this.InBcValueIndex = GetIndexFromXml(row2, "Input", -1);
                        this.PanelBcValueIndex = GetIndexFromXml(row2, "Panel", this.PanelBcValueIndex);
                    }

                }
                catch (Exception ex)
                {
                    // 例外が発生した場合は初期化する
                    this.Initialize();
                    ExceptionMessage(ex, "設定の読み込みでエラーが発生しました。[Config.Load]", true);
                }
            }
            else
            {
                // 設定ファイルがなかった場合
            }

        }

        public int GetIndexFromXml(XElement row, string ColumName, int _default)
        {
            if (row.Element(ColumName) != null && row.Element(ColumName).Value != null)
            {
                if (int.TryParse(row.Element(ColumName).Value, out int i) == true)
                {
                    if (i < 0 || i > 255)
                    {
                        return -1;
                    }
                    else
                    {
                        return i;
                    }
                }
                else
                {
                    return _default;
                }
            }
            else
            {
                return _default;
            }

        }

        public int GetIndexFromXmlDirect(XElement row, int _default)
        {
            if (row != null && row.Value != null)
            {
                if (int.TryParse(row.Value, out int i) == true)
                {
                    if (i < 0 || i > 255)
                    {
                        return -1;
                    }
                    else
                    {
                        return i;
                    }
                }
                else
                {
                    return _default;
                }
            }
            else
            {
                return _default;
            }

        }

#endregion


    }
}
