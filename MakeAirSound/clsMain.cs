﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.InteropServices;
using System.Text;
using System.Windows.Forms;
using System.Xml.Linq;

using AtsPlugin.Importing;
using AtsPlugin.MotorNoise;
using AtsPlugin.Parametrics;

namespace AtsPlugin
{
    public class clsMain
    {
        #region 定義

        #region ハンドル位置保存クラス
        private class MyAtsHandles
        {
            #region 変数・プロパティ定義
            public int Power;
            public int Brake;
            public int Reverser;
            public int ConstantSpeed;

            private int pEmgBrake;

            public bool IsEmgBrake
            {
                get { if (this.Brake == this.pEmgBrake) return true; else return false; }
            }
            #endregion

            public AtsPlugin.AtsHandles Handles
            {
                get { return new AtsPlugin.AtsHandles() { Power = this.Power, Brake = this.Brake, Reverser = this.Reverser, ConstantSpeed = this.ConstantSpeed }; }
            }

            #region コンストラクタ
            /// <summary>
            /// コンストラクタ
            /// </summary>
            public MyAtsHandles()
            {
                this.Power = 0;
                this.Brake = 0;
                this.Reverser = 0;
                this.ConstantSpeed = 0;

            }
            public MyAtsHandles(int _EmgBrake, int _InitialHandle)
            {
                this.Power = 0;
                if (_InitialHandle == 0)
                {
                    // SVC
                    this.Brake = 0;
                }
                else
                {
                    this.Brake = _EmgBrake;
                }
                this.Reverser = 0;
                this.ConstantSpeed = 0;
                this.pEmgBrake = _EmgBrake;
            }
            #endregion

            #region Get
            public AtsPlugin.AtsHandles GetHandles()
            {
                return new AtsPlugin.AtsHandles() { Power = this.Power, Brake = this.Brake, Reverser = this.Reverser, ConstantSpeed = this.ConstantSpeed };
            }
            #endregion
        }
        #endregion

        #region 車両状態保存クラス
        public class MyVehicleState
        {
            #region 変数定義
            private bool pIsFirst = true;

            private AtsPlugin.AtsVehicleState pNewState;
            public AtsPlugin.AtsVehicleState NewState
            {
                get { return this.pNewState; }
            }

            private AtsPlugin.AtsVehicleState pOldState;

            private bool pPilotLamp;
            public bool PilotLamp
            {
                get { return this.pPilotLamp; }
            }

            private double pLocation;
            public double Location
            {
                get { return Math.Abs(this.pLocation); }
            }

            /// <summary>
            /// 加速度[km/h/s]
            /// </summary>
            private float pAccelaration;
            /// <summary>
            /// 加速度[km/h/s]
            /// </summary>
            public float Accelaration
            {
                get { return this.pAccelaration; }
            }

            private float pOldAccelaration;

            private float pOutBc;
            public float OutBc
            {
                get { return this.pOutBc; }
            }

            private float pOldOutBc;
            public float OldOutBc
            {
                get { return this.pOldOutBc; }
            }


            #endregion

            #region コンストラクタ
            public MyVehicleState()
            {
                this.pIsFirst = true;
                this.pNewState = new AtsPlugin.AtsVehicleState();
                this.pOldState = new AtsPlugin.AtsVehicleState();
                this.pPilotLamp = true;
                this.pAccelaration = 0;
                this.pOldAccelaration = 0;
                this.pOutBc = 0;
                this.pOldOutBc = 0;
            }
            #endregion

            #region セット
            public void SetState(AtsPlugin.AtsVehicleState vehicleState)
            {
                if (this.pIsFirst == true)
                {
                    this.pOldState = vehicleState;
                    this.pIsFirst = false;
                }
                else
                {
                    this.pOldState = this.pNewState;
                    this.pOldAccelaration = this.pAccelaration;
                    this.pAccelaration = this.pNewState.Speed - this.pOldState.Speed;
                }

                this.pNewState = vehicleState;
                this.pLocation += this.GetDeltaLocation();
                this.pOldOutBc = this.pOutBc;
            }

            /// <summary>
            /// BVE側の戸閉状態を更新
            /// </summary>
            /// <param name="_IsClose"></param>
            public void SetDoorClose(bool _IsClose)
            {
                this.pPilotLamp = _IsClose;
            }

            public void SetLocation(double _Location)
            {
                this.pLocation = _Location;
            }

            /// <summary>
            /// 出力用BC圧を更新
            /// </summary>
            /// <param name="_Bc"></param>
            public void SetBc(float _Bc)
            {
                this.pOutBc = _Bc;
            }
            #endregion

            #region Get
            /// <summary>
            /// 前フレームとの時間差[ms]
            /// </summary>
            /// <returns></returns>
            public int GetDeltaTime()
            {
                if (this.pIsFirst == true)
                {
                    return 0;
                }
                else
                {
                    return (this.pNewState.Time - this.pOldState.Time + 60 * 60 * 24 * 1000) % (60 * 60 * 24 * 1000);
                }
            }

            /// <summary>
            /// 前フレームとの距離差[m]
            /// </summary>
            /// <returns></returns>
            public float GetDeltaLocation()
            {
                if (this.pIsFirst == true)
                {
                    return 0;
                }
                else
                {
                    return (float)(this.pNewState.Location - this.pOldState.Location);
                }
            }

            /// <summary>
            /// 前フレームとの加速度変化量[km/h/s2]
            /// </summary>
            /// <returns></returns>
            public float GetDeltaAccelaration()
            {
                if (this.pIsFirst == true)
                {
                    return 0;
                }
                else
                {
                    return (float)(this.pAccelaration - this.pOldAccelaration);
                }
            }

            /// <summary>
            /// 前回と同じ時間帯か
            /// </summary>
            /// <param name="_Interval">時間帯間隔[ms]</param>
            /// <returns></returns>
            public bool GetAnotherMoment(int _Interval)
            {
                if (_Interval > 0)
                {
                    if ((int)(this.pNewState.Time / _Interval) == (int)(this.pOldState.Time / _Interval))
                    {
                        return true;
                    }
                    else
                    {
                        return false;
                    }
                }
                else
                {
                    return false;
                }
            }

            /// <summary>
            /// 指定した間隔の前半か後半か (前半ならtrue)
            /// </summary>
            /// <param name="_Interval">時間帯間隔[ms]</param>
            /// <returns></returns>
            public bool GetOddInterval(int _Interval)
            {
                if (_Interval > 1)
                {
                    if ((this.pNewState.Time % _Interval) < (int)(_Interval / 2))
                    {
                        return true;
                    }
                    else
                    {
                        return false;
                    }
                }
                else
                {
                    return true;
                }
            }

            /// <summary>
            /// 時を取得
            /// </summary>
            /// <returns></returns>
            public int GetHour()
            {
                return (int)(this.pNewState.Time / 3600000);
            }

            /// <summary>
            /// 分を取得
            /// </summary>
            /// <returns></returns>
            public int GetMinute()
            {
                return (int)((int)(this.pNewState.Time / 1000) / 60) % 60;
            }

            /// <summary>
            /// 秒を取得
            /// </summary>
            /// <returns></returns>
            public int GetSecond()
            {
                return (int)(this.pNewState.Time / 1000) % 60;
            }

            public int GetBc()
            {
                return (int)(this.pNewState.BcPressure);
            }

            public int GetMr()
            {
                return (int)(this.pNewState.MrPressure);
            }

            #endregion
        }
        #endregion

        #endregion

        #region 変数定義
        private string LogText = "";

        /// <summary>
        /// 本体
        /// </summary>
        private static clsMain MyAts = new clsMain();
        /// <summary>
        /// 設定
        /// </summary>
        private clsConfig CFG;

        /// <summary>
        /// ハンドル位置入力値保存用
        /// </summary>
        private MyAtsHandles pInHandles = new MyAtsHandles();

        /// <summary>
        /// 前フレームハンドル位置入力値保存用
        /// </summary>
        private AtsPlugin.AtsHandles pBeforeInHandles;

        /// <summary>
        /// ハンドル位置出力値保存用
        /// </summary>
        private AtsPlugin.AtsHandles pOutHandles;

        // 外部取得実行済みフラグ
        private bool pGotOutHandles;

        /// <summary>
        /// 列車状態
        /// </summary>
        private MyVehicleState pMyVeshicleState;

        /// <summary>
        /// BC圧状態
        /// </summary>
        private clsAirState pMyAirState;

        /// <summary>
        /// エンジン動作音クラス
        /// </summary>
        private AtsMotorNoise EngineNoise = null;

        private AtsPlugin.AtsVehicleSpec MyVehicleSpec;

#if DEBUG
        /// <summary>
        /// デバッグ画面
        /// </summary>
        private frmDebug f;
#endif

#endregion


        #region Loadイベント
        /// <summary>
        /// プラグインが読み込まれた際に呼ばれる
        /// </summary>
        public static void Load()
        {
            try
            {
                MyAts = new clsMain();
                MyAts.Load2();
            }
            catch (Exception ex)
            {
                MessageBox.Show("プラグインの読み込み処理でエラーが発生しました。[Base.Load]\n" + ex.Message);
            }
        }

        public void Load2()
        {
            try
            {
                LogText = "ロード";
                this.CFG = new clsConfig();
                this.CFG.MainLoad(); // 設定の読み込み

#if DEBUG
                f = new frmDebug();
                f.SetText(frmDebug.LabelKind_Enum.PistonArea, this.CFG.PistonArea.ToString());
                f.SetText(frmDebug.LabelKind_Enum.BlowArea, this.CFG.BlowArea.ToString());
                f.SetText(frmDebug.LabelKind_Enum.Bane, this.CFG.Bane.ToString());
#endif

                LogText = "ログ準備";
                EngineBase.Log.Init();
                EngineBase.Log.Write("[Load]");
                // 強制的に初期化
                Initialize2(0);
                //
                LogText = "Handles";
                this.pInHandles = new MyAtsHandles(this.MyVehicleSpec.BrakeNotches, 1);
                this.pOutHandles = new AtsPlugin.AtsHandles() { Power = 0, Brake = 0, Reverser = 0, ConstantSpeed = AtsPlugin.AtsCscInstruction.Continue };
                this.pBeforeInHandles = new AtsPlugin.AtsHandles() { Power = 0, Brake = 0, Reverser = 0, ConstantSpeed = AtsPlugin.AtsCscInstruction.Continue };

            }
            catch (Exception ex)
            {
                MessageBox.Show("プラグインの読み込み処理でエラーが発生しました。[Load]\n" + LogText + "\n" + ex.Message);
            }
        }
        #endregion

        #region Disposeイベント
        /// <summary>
        /// 別のプラグインを読み込むときもしくはBVEが終了する際に呼ばれる
        /// </summary>
        public static void Dispose()
        {
            try
            {
                MyAts.Dispose2();
                MyAts = null;
            }
            catch (Exception ex)
            {
                MessageBox.Show("プラグインのお片付け処理でエラーが発生しました。[Base.Dispose]\n" + ex.Message);
            }
        }

        public void Dispose2()
        {
            this.CFG = null;

            if (this.EngineNoise != null)
            {
                this.EngineNoise = null;
            }

            EngineBase.Log.Write("[Dispose]");
            EngineBase.Log.Dispose();
        }
        #endregion

        #region SetVehicleSpecイベント
        /// <summary>
        /// 車両読み込み時に呼び出される
        /// </summary>
        /// <param name="vehicleSpec"></param>
        public static void SetVehicleSpec(AtsPlugin.AtsVehicleSpec vehicleSpec)
        {
            try
            {
                MyAts.SetVehicleSpec2(vehicleSpec);
            }
            catch (Exception ex)
            {
                MessageBox.Show("プラグインの車両読み込み処理でエラーが発生しました。[Base.SetVehicleSpec]\n" + ex.Message);
            }
        }

        /// <summary>
        /// 車両読み込み時に呼び出される
        /// </summary>
        /// <param name="vehicleSpec"></param>
        public void SetVehicleSpec2(AtsPlugin.AtsVehicleSpec vehicleSpec)
        {
            try
            {
                this.MyVehicleSpec = vehicleSpec;
            }
            catch (Exception ex)
            {
                MessageBox.Show("車両の読み込み処理でエラーが発生しました。[SetVehicleSpec]\n" + ex.Message);
            }
        }
        #endregion

        #region Initializeイベント
        /// <summary>
        /// シナリオ読み込み後および駅ジャンプ時に呼ばれる
        /// </summary>
        /// <param name="initialHandlePosition"></param>
        public static void Initialize(int initialHandlePosition)
        {
            try
            {
                if (MyAts is null)
                {
                    MyAts = new clsMain();
                    MyAts.Load2();
                }
                MyAts.Initialize2(initialHandlePosition);
            }
            catch (Exception ex)
            {
                MessageBox.Show("プラグインの初期化処理でエラーが発生しました。[Base.Initialize]\n" + ex.Message);
            }
        }

        public void Initialize2(int initialHandlePosition)
        {
            try
            {
                // オーディオの初期化
                AtsMotorNoise.Startup();

                if (System.IO.File.Exists(this.CFG.MotorNoiseTxtPath) == true && System.IO.File.Exists(this.CFG.SoundTxtPath) == true)
                {
                    // 音声インスタンス
                    EngineNoise = AtsMotorNoiseImporter.LoadAsset(this.CFG.MotorNoiseTxtPath, this.CFG.SoundTxtPath, this.CFG.EngineSectionName);
                }

                LogText = "VehicleState";
                this.pMyVeshicleState = new MyVehicleState();
                this.pGotOutHandles = false;

                this.pMyAirState = new clsAirState();
                this.pMyAirState.SetSetting(this.CFG.PistonArea, this.CFG.SylinderLength, this.CFG.BlowArea, this.CFG.Bane, this.CFG.MaxA);
#if DEBUG
                this.pMyAirState.f = f;
#endif

            }
            catch (Exception ex)
            {
                MessageBox.Show("初期化処理でエラーが発生しました。[Initialize]\n" + ex.Message);
                EngineBase.Log.Write("[Initialize] (Error) " + LogText + ":" + ex.Message);
            }
        }
        #endregion

        #region Elapseイベント
        /// <summary>
        /// 毎フレーム
        /// </summary>
        /// <param name="vehicleState">車両状態</param>
        /// <param name="panel">パネルの出力配列のポインタ</param>
        /// <param name="sound">サウンドの出力配列のポインタ</param>
        /// <returns>各ハンドル位置</returns>
        public static AtsPlugin.AtsHandles Elapse(AtsPlugin.AtsVehicleState vehicleState, IntPtr panel, IntPtr sound)
        {
            return MyAts.Elapse2(vehicleState, panel, sound);
        }

        public AtsPlugin.AtsHandles Elapse2(AtsPlugin.AtsVehicleState vehicleState, IntPtr panel, IntPtr sound)
        {
            try
            {
                // ハンドル
                this.pOutHandles = this.pInHandles.GetHandles();
                // - 外部値の取得
                SetOutHandlesValue(ref this.pOutHandles, panel);

                // 列車状態
                this.pMyVeshicleState.SetState(vehicleState);


                // BC圧取得
                double P;

                if (this.CFG.InBcValueIndex == -1)
                {
                    // - BVEから
                    P = (double)this.pMyVeshicleState.NewState.BcPressure;
                }
                else
                {
                    // - 別プラグインから
                    P = (double)GetPanelValue(panel, this.CFG.InBcValueIndex, 0);
                    this.pMyVeshicleState.SetBc((float)P);
                }
#if DEBUG
                f.SetText(frmDebug.LabelKind_Enum.BcPressure, P.ToString("0"));
#endif

                // 現在値計算
                if (this.pMyAirState.SetState((double)this.pMyVeshicleState.GetDeltaTime(), P) == true)
                {
                    // 排気速度
                    //double speed = this.pMyAirState.CalcExhaustSpeed();
                    double speed = this.pMyAirState.Speed;
#if DEBUG
                    f.SetText(frmDebug.LabelKind_Enum.BlowSpeed, speed.ToString("0.0000"));
#endif

                    // - 音声
                    if (this.EngineNoise != null)
                    {
                        this.EngineNoise.DirectionMixtureRatio = 1.0f;
                        // ピッチ
                        this.EngineNoise.Position = (float)speed;
                        // ボリューム
                        this.EngineNoise.Volume = 1.0f;
                        //
                        this.EngineNoise.Update();
                    }

                    if (this.CFG.DebugMode == true && speed != 0)
                    {
                        EngineBase.Log.Write("BC="+ P.ToString("0.00")+" dt="+ this.pMyVeshicleState.GetDeltaTime().ToString()+" BlowSpeed=" + speed.ToString("0.0000"));
                    }

                }
                else
                {
                    // 失敗
                    // - 音声
                    if (this.EngineNoise != null)
                    {
                        this.EngineNoise.DirectionMixtureRatio = 1.0f;
                        // ピッチ
                        this.EngineNoise.Position = 0;
                        // ボリューム
                        this.EngineNoise.Volume = 0;
                        //
                        this.EngineNoise.Update();
                    }
                }

                this.SetPanelAssign(panel);
            }
            catch (Exception ex)
            {
#if DEBUG
                MessageBox.Show(ex.Message, "AtsPT4s.Elapse");
#endif
                EngineBase.Log.Write("[Elapse] (Error) " + ex.Message);
            }

            // 返却
            return this.pOutHandles;
        }

        #region 外部ハンドル値の取得
        private void SetOutHandlesValue(ref AtsPlugin.AtsHandles OutHandles, IntPtr panel)
        {
            try
            {
                // Power
                if (this.CFG.InPowerHandleIndex != -1)
                {
                    // 負だとマズイので絶対値に変換
                    OutHandles.Power = Math.Abs(Marshal.ReadInt32(panel, 4 * this.CFG.InPowerHandleIndex));
                }

                // Brake
                if (this.CFG.InBrakeHandleIndex != -1)
                {
                    // 負だとマズイので絶対値に変換
                    OutHandles.Brake = Math.Abs(Marshal.ReadInt32(panel, 4 * this.CFG.InBrakeHandleIndex));
                }

                // Reverser
                if (this.CFG.InReverserHandleIndex != -1)
                {
                    // 負はOK
                    OutHandles.Reverser = Marshal.ReadInt32(panel, 4 * this.CFG.InReverserHandleIndex);
                }

                // CSC
                if (this.CFG.InCscValueIndex != -1)
                {
                    // 負だとマズイので絶対値に変換
                    OutHandles.ConstantSpeed = Math.Abs(Marshal.ReadInt32(panel, 4 * this.CFG.InCscValueIndex));
                }

            }
            catch (Exception ex)
            {
                EngineBase.Log.Write("[SetOutHandlesValue] (Error) " + ex.Message);
            }
        }
        #endregion

        #region パネルのセット
        private void SetPanelAssign(IntPtr panel)
        {
            try
            {
                // [Default 115] BC
                SetPanelValue(panel, this.CFG.PanelBcValueIndex, this.pMyVeshicleState.GetBc());


                // [Default 200] レバーサ出力値
                SetPanelValue(panel, this.CFG.OutReverserHandleIndex, this.pOutHandles.Reverser);

                // [Default 201] パワー出力値
                SetPanelValue(panel, this.CFG.OutPowerHandleIndex, this.pOutHandles.Power);

                // [Default 202] ブレーキ出力値
                SetPanelValue(panel, this.CFG.OutBrakeHandleIndex, this.pOutHandles.Brake);

                // [Default 203] 定速出力値
                SetPanelValue(panel, this.CFG.OutCscValueIndex, this.pOutHandles.ConstantSpeed);


            }
            catch (Exception ex)
            {
#if DEBUG
                MessageBox.Show(ex.Message, "AtsPT4s.SetPanelAssign");
#else
                Console.WriteLine("[SetPanel]\n" + ex.Message);
#endif
                EngineBase.Log.Write("[SetPanelAssign] (Error) " + ex.Message);
            }
        }

        private void SetPanelValue(IntPtr panel, int _Index, int _Value)
        {
            if (_Index >= 0 && _Index <= 255)
            {
                const int SIZE_OF_TYPE = 4;
                Marshal.WriteInt32(panel, SIZE_OF_TYPE * _Index, _Value);
            }
        }

        private int GetPanelValue(IntPtr panel, int _Index, int _Value)
        {
            if (_Index >= 0 && _Index <= 255)
            {
                const int SIZE_OF_TYPE = 4;
                return Marshal.ReadInt32(panel, SIZE_OF_TYPE * _Index);
            }
            else
            {
                return _Value;
            }
        }

        #endregion

        #region サウンドのセット
        private void SetSoundAssign(IntPtr sound)
        {
            try
            {

            }
            catch (Exception ex)
            {
#if DEBUG
                MessageBox.Show(ex.Message, "AtsPT4s.SetSoundAssign");
#else
                Console.WriteLine("[SetSound]\n" + ex.Message);
#endif
                EngineBase.Log.Write("[SetSoundAssign] (Error) " + ex.Message);
            }
        }

        private void SetSoundValue(IntPtr sound, int _Index, int _Value)
        {
            if (_Index >= 0 && _Index <= 255)
            {
                const int SIZE_OF_TYPE = 4;
                Marshal.WriteInt32(sound, SIZE_OF_TYPE * _Index, _Value);
            }
        }

        private int ReadSoundValue(IntPtr sound, int _Index)
        {
            if (_Index >= 0 && _Index <= 255)
            {
                const int SIZE_OF_TYPE = 4;
                return Marshal.ReadInt32(sound, SIZE_OF_TYPE * _Index);
            }
            else
            {
                return -1;
            }
        }
        #endregion


        #endregion

        #region SetPowerイベント
        /// <summary>
        /// BVEへの力行入力値が変化した時に呼ばれる
        /// </summary>
        /// <param name="handlePosition"></param>
        public static void SetPower(int handlePosition)
        {
            try
            {
                MyAts.SetPower2(handlePosition);
            }
            catch (Exception ex)
            {
                MessageBox.Show("力行のセットでエラーが発生しました。[Base.SetPower]\n" + ex.Message);
            }
        }

        public void SetPower2(int handlePosition)
        {
            try
            {
                if (this.CFG.InPowerHandleIndex == -1 && this.pGotOutHandles == false)
                {
                    this.pInHandles.Power = handlePosition;
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show("力行のセットでエラーが発生しました。[SetPower]\n" + ex.Message);
                EngineBase.Log.Write("[SetPower] (Error) " + ex.Message);
            }

        }
        #endregion

        #region SetBrakeイベント
        /// <summary>
        /// BVEへのブレーキ入力値が変化した時に呼ばれる
        /// </summary>
        /// <param name="handlePosition"></param>
        public static void SetBrake(int handlePosition)
        {
            try
            {
                if (MyAts != null)
                {
                    MyAts.SetBrake2(handlePosition);
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show("ブレーキのセットでエラーが発生しました。[Base.SetBrake]\n" + ex.Message);
            }
        }

        public void SetBrake2(int handlePosition)
        {
            try
            {
                if (this.CFG.InBrakeHandleIndex == -1 && this.pGotOutHandles == false)
                {
                    this.pInHandles.Brake = handlePosition;
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show("ブレーキのセットでエラーが発生しました。[SetBrake]\n" + ex.Message);
                EngineBase.Log.Write("[SetBrake] (Error) " + ex.Message);
            }
        }
        #endregion

        #region SetReverserイベント
        /// <summary>
        /// BVEへの逆転機位置入力値が変化した時に呼ばれる
        /// </summary>
        /// <param name="handlePosition"></param>
        public static void SetReverser(int handlePosition)
        {
            try
            {
                MyAts.SetReverser2(handlePosition);
            }
            catch (Exception ex)
            {
                MessageBox.Show("レバーサのセットでエラーが発生しました。[Base.SetReverser]\n" + ex.Message);
            }
        }

        public void SetReverser2(int handlePosition)
        {
            try
            {
                if (this.CFG.InReverserHandleIndex == -1 && this.pGotOutHandles == false)
                {
                    this.pInHandles.Reverser = handlePosition;
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show("レバーサのセットでエラーが発生しました。[SetReverser]\n" + ex.Message);
                EngineBase.Log.Write("[SetReverser] (Error) " + ex.Message);
            }
        }
        #endregion

        #region Keyイベント

        #region KeyDownイベント
        /// <summary>
        /// BVE本体のKeyDownイベントでATSキーが押されたことが検知された場合に呼ばれる
        /// </summary>
        /// <param name="keyIndex"></param>
        public static void KeyDown(int keyIndex)
        {
            MyAts.KeyDown2(keyIndex);
        }

        //public void KeyDown2(int keyIndex)
        //{
        //    switch ((AtsPlugin.AtsKey)keyIndex)
        //    {
        //        case AtsPlugin.AtsKey.S:
        //            // [Space]
        //            break;

        //        case AtsPlugin.AtsKey.A1:
        //            // [Insert]
        //            break;

        //        case AtsPlugin.AtsKey.A2:
        //            // [Delete]
        //            break;

        //        case AtsPlugin.AtsKey.B1:
        //            // [Home]
        //            break;

        //        case AtsPlugin.AtsKey.B2:
        //            // [End]
        //            break;

        //        case AtsPlugin.AtsKey.C1:
        //            // [PageUp]
        //            break;

        //        case AtsPlugin.AtsKey.C2:
        //            // [PageDown]
        //            break;

        //        case AtsPlugin.AtsKey.D:
        //            // [2]
        //            break;

        //        case AtsPlugin.AtsKey.E:
        //            // [3]
        //            break;

        //        case AtsPlugin.AtsKey.F:
        //            // [4]
        //            break;

        //        case AtsPlugin.AtsKey.G:
        //            // [5]
        //            break;

        //        case AtsPlugin.AtsKey.H:
        //            // [6]
        //            break;

        //        case AtsPlugin.AtsKey.I:
        //            // [7]
        //            break;

        //        case AtsPlugin.AtsKey.J:
        //            // [8]
        //            break;

        //        case AtsPlugin.AtsKey.K:
        //            // [9]
        //            break;

        //        case AtsPlugin.AtsKey.L:
        //            // [0]
        //            break;

        //        default:
        //            break;
        //    }
        //}

        public void KeyDown2(int keyIndex)
        {
            try
            {

                // CheckKeyPush(this.CFG.KeyMode[keyIndex]);
            }
            catch (Exception ex)
            {
                MessageBox.Show("ATSキー押下検知でエラーが発生しました。[KeyDown]\n" + ex.Message);
            }
        }

        #endregion

        #region KeyUpイベント
        /// <summary>
        /// BVE本体のKeyUpイベントでATSキーが離されたことが検知された場合に呼ばれる
        /// </summary>
        /// <param name="keyIndex"></param>
        public static void KeyUp(int keyIndex)
        {
            MyAts.KeyUp2(keyIndex);
        }

        public void KeyUp2(int keyIndex)
        {
            try
            {

                //CheckKeyRelease(this.CFG.KeyMode[keyIndex]);
            }
            catch (Exception ex)
            {
                MessageBox.Show("ATSキー離上検知で発生しました。[KeyUp]\n" + ex.Message);
            }
        }

        #endregion

        #endregion

        #region HornBlowイベント
        /// <summary>
        /// 警笛キーが押されている時に呼ばれる
        /// </summary>
        /// <param name="hornIndex"></param>
        public static void HornBlow(int hornIndex)
        {
            try
            {
                MyAts.HornBlow2(hornIndex);
            }
            catch (Exception ex)
            {
                MessageBox.Show("警笛の処理でエラーが発生しました。[Base.HornBlow]\n" + ex.Message);
            }
        }

        public void HornBlow2(int hornIndex)
        {
            try
            {

            }
            catch (Exception ex)
            {
                MessageBox.Show("警笛の処理でエラーが発生しました。[HornBlow]\n" + ex.Message);
                EngineBase.Log.Write("[HornBlow] (Error) " + ex.Message);
            }
        }
        #endregion

        #region DoorOpenイベント
        /// <summary>
        /// ドアが開いた時に呼ばれる
        /// </summary>
        public static void DoorOpen()
        {
            try
            {
                MyAts.DoorOpen2();
            }
            catch (Exception ex)
            {
                MessageBox.Show("戸開処理でエラーが発生しました。[Base.DoorOpen]\n" + ex.Message);
            }
        }

        public void DoorOpen2()
        {
            try
            {

            }
            catch (Exception ex)
            {
                MessageBox.Show("戸開処理でエラーが発生しました。[DoorOpen]\n" + ex.Message);
                EngineBase.Log.Write("[DoorOpen] (Error) " + ex.Message);
            }
        }
        #endregion

        #region DoorCloseイベント
        /// <summary>
        /// 全車のドアが閉じた時に呼ばれる
        /// </summary>
        public static void DoorClose()
        {
            try
            {
                MyAts.DoorClose2();
            }
            catch (Exception ex)
            {
                MessageBox.Show("戸開処理でエラーが発生しました。[Base.DoorClose]\n" + ex.Message);
            }
        }

        public void DoorClose2()
        {
            try
            {

            }
            catch (Exception ex)
            {
                MessageBox.Show("戸開処理でエラーが発生しました。[DoorClose]\n" + ex.Message);
                EngineBase.Log.Write("[DoorClose] (Error) " + ex.Message);
            }
        }
        #endregion

        #region SetSignalイベント
        /// <summary>
        /// 現在の閉塞の信号インデックスが変化した時に呼ばれる
        /// </summary>
        /// <param name="signalIndex"></param>
        public static void SetSignal(int signalIndex)
        {
            try
            {
                MyAts.SetSignal2(signalIndex);
            }
            catch (Exception ex)
            {
                MessageBox.Show("信号処理でエラーが発生しました。[Base.SetSignal]\n" + ex.Message);
            }
        }

        public void SetSignal2(int _SignalIndex)
        {
            try
            {
               
            }
            catch (Exception ex)
            {
                MessageBox.Show("信号処理でエラーが発生しました。[Base.SetSignal2]\n" + ex.Message);
                EngineBase.Log.Write("[SetSignal] (Error) " + ex.Message);
            }
        }
        #endregion

        #region SetBeaconDataイベント
        /// <summary>
        /// 地上子を通過した時に呼ばれる
        /// </summary>
        /// <param name="beaconData"></param>
        public static void SetBeaconData(AtsPlugin.AtsBeaconData beaconData)
        {
            try
            {
                MyAts.SetBeaconData2(beaconData);
            }
            catch (Exception ex)
            {
                MessageBox.Show("地上子データ取得の処理でエラーが発生しました。[Base.SetBeaconData]\n" + ex.Message);
            }
        }

        public void SetBeaconData2(AtsPlugin.AtsBeaconData beaconData)
        {
            try
            {

            }
            catch (Exception ex)
            {
                MessageBox.Show("地上子データ取得の処理でエラーが発生しました。[SetBeaconData]\n" +
                                "　地上子番号 : " + beaconData.Type.ToString() + "\n" +
                                "　信号値     : " + beaconData.Signal.ToString() + "\n" +
                                "　距離to閉塞 : " + beaconData.Distance.ToString() + "\n" +
                                "　オプション : " + beaconData.Optional.ToString() + "\n" +
                                ex.Message);
                EngineBase.Log.Write("[SetBeaconData] (Error) " + ex.Message);
            }
        }
        #endregion


        #region 【オリジナル】SetHandlesイベント
        public static void SetHandles(int PowerHandle, int BrakeHandle, int ReverserHandle, int CscValue)
        {
            try
            {
                MyAts.SetHandles2(PowerHandle, BrakeHandle, ReverserHandle, CscValue);
            }
            catch (Exception ex)
            {
                MessageBox.Show("外部ハンドル位置取得の処理でエラーが発生しました。[Base.SetHandles]\n" + ex.Message);
            }
        }

        public void SetHandles2(int PowerHandle, int BrakeHandle, int ReverserHandle, int CscValue)
        {
            try
            {
                this.pGotOutHandles = true;
                this.pOutHandles.Power = PowerHandle;
                this.pOutHandles.Brake = PowerHandle;
                this.pOutHandles.Reverser = ReverserHandle;
                this.pOutHandles.ConstantSpeed = CscValue;
            }
            catch (Exception ex)
            {
                MessageBox.Show("外部ハンドル位置取得の処理でエラーが発生しました。[SetHandles]\n" + ex.Message);
                EngineBase.Log.Write("[SetHandle] (Error) " + ex.Message);
            }
        }
        #endregion


    }
}
