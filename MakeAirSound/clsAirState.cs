﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
//using System.Threading.Tasks;

namespace AtsPlugin
{
    public class clsAirState
    {
        #region 定数
        /// <summary>
        /// 気体定数 [J/K*mol]
        /// </summary>
        public const double R = 8.314;

        /// <summary>
        /// 絶対温度(0℃) [K]
        /// </summary>
        public const double T = 273.15;

        /// <summary>
        /// 標準気圧[kPa]
        /// </summary>
        public const double P0 = 101.325;
        #endregion

        #region メンバ定義

        #region - 設定
        /// <summary>
        /// ピストン面積[m2]
        /// </summary>
        public double PistonArea;
        /// <summary>
        /// 排気管面積[m2]
        /// </summary>
        public double AirDuctArea;
        /// <summary>
        /// ばね係数
        /// </summary>
        public double Spring;
        /// <summary>
        /// ピストン初期長[m]
        /// </summary>
        public double PistonLength;

        public double MaxA;
        #endregion

        #region - 状態
        private double pDeltaT;

        private double pNowPressure;
        private double pPastPressure;

        private double pNowPistonLength;
        private double pPastPistonLength;

        private double pNowMol;
        private double pPastMol;

        private double pSpeed;
        /// <summary>
        /// 排気スピード(Mol/s)
        /// </summary>
        public double Speed
        {
            get { return this.pSpeed; }
        }

#if DEBUG
        public frmDebug f = null;
#endif

#endregion

        #region コンストラクタ
        public clsAirState()
        {
            Initialize();
        }
        #endregion

        #region 初期化
        public void Initialize()
        {
            this.PistonArea = 0;
            this.AirDuctArea = 0;
            this.Spring = 0;
            this.PistonLength = 0;

            this.pDeltaT = 0;
            this.pNowPressure = P0;
            this.pPastPressure = P0;
            this.pNowPistonLength = 0;
            this.pPastPistonLength = 0;
            this.pNowMol = 0;
            this.pPastMol = 0;

            this.pSpeed = 0;
        }
        #endregion

        #region 設定
        public bool SetSetting(double _PistonArea, double _PistonLength, double _AirDuctArea, double _Spring, double _MaxA)
        {
            try
            {
                this.PistonArea = _PistonArea;
                this.PistonLength = _PistonLength;
                this.AirDuctArea = _AirDuctArea;
                this.Spring = _Spring;
                this.MaxA = _MaxA;

                this.pNowPistonLength = this.PistonLength;
                this.pPastPistonLength = this.PistonLength;

                this.pNowMol = CalcMol(this.pNowPressure, this.PistonArea, this.pNowPistonLength);
                this.pPastMol = this.pNowMol;

#if DEBUG
                if (f != null)
                {
                    f.SetText(frmDebug.LabelKind_Enum.R, R.ToString());
                    f.SetText(frmDebug.LabelKind_Enum.T, T.ToString());
                }
#endif
                return true;
            }
            catch (Exception ex)
            {
                EngineBase.Log.Write("[SetSetting] " + ex.Message);
                return false;
            }
        }
        #endregion

        #region 状態方程式

        #region - PV -> n
        private double CalcMol(double _P, double _S, double _L)
        {
            try
            {
                return CalcMol(_P, (_S * _L));
            }
            catch (Exception ex)
            {
                EngineBase.Log.Write("[CalcMol] " + ex.Message);
                return 0;
            }
        }

        private double CalcMol(double _P, double _V)
        {
            try
            {
                return _P * _V / (R * T);
            }
            catch (Exception ex)
            {
                EngineBase.Log.Write("[CalcMol] " + ex.Message);
                return 0;
            }
        }
        #endregion

        #region nP -> V
        private double CaclVolume(double _P, double _M)
        {
            try
            {
                return (_M * R * T) / _P;
            }
            catch (Exception ex)
            {
                EngineBase.Log.Write("[CaclVolume] " + ex.Message);
                return 0;
            }
        }
        #endregion

        #endregion


        #region 現在状態のセット
        public bool SetState(double _DeltaT, double _P)
        {
            try
            {
                // バックアップ
                this.pPastPressure = this.pNowPressure;
                this.pPastPistonLength = this.pNowPistonLength;
                this.pPastMol = this.pNowMol;

                // 時間差セット
                this.pDeltaT = _DeltaT;

                // 現在の圧力
                this.pNowPressure = P0 + _P;

                // バネの変化量[m]
                double dL = this.PistonArea * _P / this.Spring;
                this.pNowPistonLength = this.PistonLength + dL;

                // 現在のモル量[mol]
                this.pNowMol = CalcMol(this.pNowPressure, this.PistonArea, this.pNowPistonLength);

#if DEBUG
                if (f != null)
                {
                    f.SetText(frmDebug.LabelKind_Enum.Mol, this.pNowMol.ToString("0.0000"));
                    f.SetText(frmDebug.LabelKind_Enum.DeltaMol, (this.pNowMol - this.pPastMol).ToString("0.0000"));
                    f.SetText(frmDebug.LabelKind_Enum.PistonLength, this.pNowPistonLength.ToString("0.0000"));
                }
#endif
                double s = CalcExhaustSpeed();
                if (s > this.pSpeed)
                {
                    if (s > this.pSpeed + this.MaxA * (double)this.pDeltaT * 0.001)
                    {
                        this.pSpeed += this.MaxA * (double)this.pDeltaT * 0.001;
                    }
                    else
                    {
                        this.pSpeed = s;
                    }
                }
                else
                {
                    if (s < this.pSpeed - this.MaxA * (double)this.pDeltaT * 0.001)
                    {
                        this.pSpeed -= this.MaxA * (double)this.pDeltaT * 0.001;
                    }
                    else
                    {
                        this.pSpeed = s;
                    }
                }

                return true;
            }
            catch (Exception ex)
            {
                EngineBase.Log.Write("[SetState] " + ex.Message);
                return false;
            }

        }
        #endregion

        #region 排気速度
        public double CalcExhaustSpeed()
        {
            try
            {
                double speed = 0;

                if (this.pNowMol < this.pPastMol)
                {
                    // 変化量 (kPaを使って計算しているので1000倍する)
                    double dV = CaclVolume(P0, (this.pPastMol - this.pNowMol)) * 1000.0;
                    // 速度
                    speed = dV / this.pDeltaT * 1000.0;

                    return speed;
                }
                else
                {
                    return 0;
                }

            }
            catch (Exception ex)
            {
                EngineBase.Log.Write("[CalcExhaustSpeed] " + ex.Message);
                return 0;
            }
        }
        #endregion

        #endregion

    }
}
