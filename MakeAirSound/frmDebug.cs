﻿using System;
using System.Collections.Generic;

using System.Linq;
using System.Text;
//using System.Threading.Tasks;
using System.Windows.Forms;

namespace AtsPlugin
{
#if DEBUG
    public class frmDebug : Form
    {
#region ENUM
        public enum LabelKind_Enum
        {
            BcPressure,
            Volume,
            PistonArea,
            PistonLength,
            Mol,
            R,
            T,
            Bane,
            BlowArea,
            DeltaMol,
            BlowSpeed
        }

#endregion


#region コントロール定義
        private TabControl tabControl1;
        private TabPage tabMain;
        private System.ComponentModel.IContainer components;
        private GroupBox groupBox1;
        private Label lblPistonLengthV;
        private Label lblPistonAreaV;
        private Label lblVolumeV;
        private Label lblPressureV;
        private Label lblPistonLengthT;
        private Label lblPistonAreaT;
        private Label lblVolumeT;
        private Label lblBlowSpeedV;
        private Label lblBlowSpeedT;
        private Label lblDMolV;
        private Label lblDMolT;
        private Label lblTempratureV;
        private Label lblTempratureT;
        private Label lblRV;
        private Label lblRT;
        private Label lblMolV;
        private Label lblMolT;
        private Label lblSpringV;
        private Label lblSpringT;
        private Label lblBlowAreaV;
        private Label lblBlowAreaT;
        private Label lblPressureT;

#endregion

#region イニシャライズ
        private void InitializeComponent()
        {
            this.tabControl1 = new System.Windows.Forms.TabControl();
            this.tabMain = new System.Windows.Forms.TabPage();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.lblBlowAreaV = new System.Windows.Forms.Label();
            this.lblBlowAreaT = new System.Windows.Forms.Label();
            this.lblSpringV = new System.Windows.Forms.Label();
            this.lblSpringT = new System.Windows.Forms.Label();
            this.lblBlowSpeedV = new System.Windows.Forms.Label();
            this.lblBlowSpeedT = new System.Windows.Forms.Label();
            this.lblDMolV = new System.Windows.Forms.Label();
            this.lblDMolT = new System.Windows.Forms.Label();
            this.lblTempratureV = new System.Windows.Forms.Label();
            this.lblTempratureT = new System.Windows.Forms.Label();
            this.lblRV = new System.Windows.Forms.Label();
            this.lblRT = new System.Windows.Forms.Label();
            this.lblMolV = new System.Windows.Forms.Label();
            this.lblMolT = new System.Windows.Forms.Label();
            this.lblPistonLengthV = new System.Windows.Forms.Label();
            this.lblPistonAreaV = new System.Windows.Forms.Label();
            this.lblVolumeV = new System.Windows.Forms.Label();
            this.lblPressureV = new System.Windows.Forms.Label();
            this.lblPistonLengthT = new System.Windows.Forms.Label();
            this.lblPistonAreaT = new System.Windows.Forms.Label();
            this.lblVolumeT = new System.Windows.Forms.Label();
            this.lblPressureT = new System.Windows.Forms.Label();
            this.tabControl1.SuspendLayout();
            this.tabMain.SuspendLayout();
            this.groupBox1.SuspendLayout();
            this.SuspendLayout();
            // 
            // tabControl1
            // 
            this.tabControl1.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.tabControl1.Controls.Add(this.tabMain);
            this.tabControl1.Location = new System.Drawing.Point(0, 0);
            this.tabControl1.Name = "tabControl1";
            this.tabControl1.SelectedIndex = 0;
            this.tabControl1.Size = new System.Drawing.Size(384, 460);
            this.tabControl1.TabIndex = 0;
            // 
            // tabMain
            // 
            this.tabMain.Controls.Add(this.groupBox1);
            this.tabMain.Location = new System.Drawing.Point(4, 22);
            this.tabMain.Name = "tabMain";
            this.tabMain.Padding = new System.Windows.Forms.Padding(3);
            this.tabMain.Size = new System.Drawing.Size(376, 434);
            this.tabMain.TabIndex = 0;
            this.tabMain.Text = "Main";
            this.tabMain.UseVisualStyleBackColor = true;
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.lblBlowAreaV);
            this.groupBox1.Controls.Add(this.lblBlowAreaT);
            this.groupBox1.Controls.Add(this.lblSpringV);
            this.groupBox1.Controls.Add(this.lblSpringT);
            this.groupBox1.Controls.Add(this.lblBlowSpeedV);
            this.groupBox1.Controls.Add(this.lblBlowSpeedT);
            this.groupBox1.Controls.Add(this.lblDMolV);
            this.groupBox1.Controls.Add(this.lblDMolT);
            this.groupBox1.Controls.Add(this.lblTempratureV);
            this.groupBox1.Controls.Add(this.lblTempratureT);
            this.groupBox1.Controls.Add(this.lblRV);
            this.groupBox1.Controls.Add(this.lblRT);
            this.groupBox1.Controls.Add(this.lblMolV);
            this.groupBox1.Controls.Add(this.lblMolT);
            this.groupBox1.Controls.Add(this.lblPistonLengthV);
            this.groupBox1.Controls.Add(this.lblPistonAreaV);
            this.groupBox1.Controls.Add(this.lblVolumeV);
            this.groupBox1.Controls.Add(this.lblPressureV);
            this.groupBox1.Controls.Add(this.lblPistonLengthT);
            this.groupBox1.Controls.Add(this.lblPistonAreaT);
            this.groupBox1.Controls.Add(this.lblVolumeT);
            this.groupBox1.Controls.Add(this.lblPressureT);
            this.groupBox1.Location = new System.Drawing.Point(8, 10);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(360, 270);
            this.groupBox1.TabIndex = 9;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "ブレーキシリンダ";
            // 
            // lblBlowAreaV
            // 
            this.lblBlowAreaV.AutoSize = true;
            this.lblBlowAreaV.Location = new System.Drawing.Point(98, 175);
            this.lblBlowAreaV.Name = "lblBlowAreaV";
            this.lblBlowAreaV.Size = new System.Drawing.Size(17, 12);
            this.lblBlowAreaV.TabIndex = 29;
            this.lblBlowAreaV.Text = "-1";
            // 
            // lblBlowAreaT
            // 
            this.lblBlowAreaT.AutoSize = true;
            this.lblBlowAreaT.Location = new System.Drawing.Point(6, 175);
            this.lblBlowAreaT.Name = "lblBlowAreaT";
            this.lblBlowAreaT.Size = new System.Drawing.Size(76, 12);
            this.lblBlowAreaT.TabIndex = 28;
            this.lblBlowAreaT.Text = "排気面積[m2]";
            // 
            // lblSpringV
            // 
            this.lblSpringV.AutoSize = true;
            this.lblSpringV.Location = new System.Drawing.Point(98, 155);
            this.lblSpringV.Name = "lblSpringV";
            this.lblSpringV.Size = new System.Drawing.Size(17, 12);
            this.lblSpringV.TabIndex = 27;
            this.lblSpringV.Text = "-1";
            // 
            // lblSpringT
            // 
            this.lblSpringT.AutoSize = true;
            this.lblSpringT.Location = new System.Drawing.Point(6, 155);
            this.lblSpringT.Name = "lblSpringT";
            this.lblSpringT.Size = new System.Drawing.Size(50, 12);
            this.lblSpringT.TabIndex = 26;
            this.lblSpringT.Text = "ばね定数";
            // 
            // lblBlowSpeedV
            // 
            this.lblBlowSpeedV.AutoSize = true;
            this.lblBlowSpeedV.Location = new System.Drawing.Point(98, 235);
            this.lblBlowSpeedV.Name = "lblBlowSpeedV";
            this.lblBlowSpeedV.Size = new System.Drawing.Size(17, 12);
            this.lblBlowSpeedV.TabIndex = 25;
            this.lblBlowSpeedV.Text = "-1";
            // 
            // lblBlowSpeedT
            // 
            this.lblBlowSpeedT.AutoSize = true;
            this.lblBlowSpeedT.Location = new System.Drawing.Point(6, 235);
            this.lblBlowSpeedT.Name = "lblBlowSpeedT";
            this.lblBlowSpeedT.Size = new System.Drawing.Size(53, 12);
            this.lblBlowSpeedT.TabIndex = 24;
            this.lblBlowSpeedT.Text = "排気速度";
            // 
            // lblDMolV
            // 
            this.lblDMolV.AutoSize = true;
            this.lblDMolV.Location = new System.Drawing.Point(98, 215);
            this.lblDMolV.Name = "lblDMolV";
            this.lblDMolV.Size = new System.Drawing.Size(17, 12);
            this.lblDMolV.TabIndex = 23;
            this.lblDMolV.Text = "-1";
            // 
            // lblDMolT
            // 
            this.lblDMolT.AutoSize = true;
            this.lblDMolT.Location = new System.Drawing.Point(6, 215);
            this.lblDMolT.Name = "lblDMolT";
            this.lblDMolT.Size = new System.Drawing.Size(53, 12);
            this.lblDMolT.TabIndex = 22;
            this.lblDMolT.Text = "⊿分子量";
            // 
            // lblTempratureV
            // 
            this.lblTempratureV.AutoSize = true;
            this.lblTempratureV.Location = new System.Drawing.Point(98, 135);
            this.lblTempratureV.Name = "lblTempratureV";
            this.lblTempratureV.Size = new System.Drawing.Size(17, 12);
            this.lblTempratureV.TabIndex = 21;
            this.lblTempratureV.Text = "-1";
            // 
            // lblTempratureT
            // 
            this.lblTempratureT.AutoSize = true;
            this.lblTempratureT.Location = new System.Drawing.Point(6, 135);
            this.lblTempratureT.Name = "lblTempratureT";
            this.lblTempratureT.Size = new System.Drawing.Size(44, 12);
            this.lblTempratureT.TabIndex = 20;
            this.lblTempratureT.Text = "温度[K]";
            // 
            // lblRV
            // 
            this.lblRV.AutoSize = true;
            this.lblRV.Location = new System.Drawing.Point(98, 115);
            this.lblRV.Name = "lblRV";
            this.lblRV.Size = new System.Drawing.Size(17, 12);
            this.lblRV.TabIndex = 19;
            this.lblRV.Text = "-1";
            // 
            // lblRT
            // 
            this.lblRT.AutoSize = true;
            this.lblRT.Location = new System.Drawing.Point(6, 115);
            this.lblRT.Name = "lblRT";
            this.lblRT.Size = new System.Drawing.Size(53, 12);
            this.lblRT.TabIndex = 18;
            this.lblRT.Text = "気体定数";
            // 
            // lblMolV
            // 
            this.lblMolV.AutoSize = true;
            this.lblMolV.Location = new System.Drawing.Point(98, 95);
            this.lblMolV.Name = "lblMolV";
            this.lblMolV.Size = new System.Drawing.Size(17, 12);
            this.lblMolV.TabIndex = 17;
            this.lblMolV.Text = "-1";
            // 
            // lblMolT
            // 
            this.lblMolT.AutoSize = true;
            this.lblMolT.Location = new System.Drawing.Point(6, 95);
            this.lblMolT.Name = "lblMolT";
            this.lblMolT.Size = new System.Drawing.Size(67, 12);
            this.lblMolT.TabIndex = 16;
            this.lblMolT.Text = "分子量[mol]";
            // 
            // lblPistonLengthV
            // 
            this.lblPistonLengthV.AutoSize = true;
            this.lblPistonLengthV.Location = new System.Drawing.Point(98, 75);
            this.lblPistonLengthV.Name = "lblPistonLengthV";
            this.lblPistonLengthV.Size = new System.Drawing.Size(17, 12);
            this.lblPistonLengthV.TabIndex = 15;
            this.lblPistonLengthV.Text = "-1";
            // 
            // lblPistonAreaV
            // 
            this.lblPistonAreaV.AutoSize = true;
            this.lblPistonAreaV.Location = new System.Drawing.Point(98, 55);
            this.lblPistonAreaV.Name = "lblPistonAreaV";
            this.lblPistonAreaV.Size = new System.Drawing.Size(17, 12);
            this.lblPistonAreaV.TabIndex = 14;
            this.lblPistonAreaV.Text = "-1";
            // 
            // lblVolumeV
            // 
            this.lblVolumeV.AutoSize = true;
            this.lblVolumeV.Location = new System.Drawing.Point(98, 35);
            this.lblVolumeV.Name = "lblVolumeV";
            this.lblVolumeV.Size = new System.Drawing.Size(17, 12);
            this.lblVolumeV.TabIndex = 13;
            this.lblVolumeV.Text = "-1";
            // 
            // lblPressureV
            // 
            this.lblPressureV.AutoSize = true;
            this.lblPressureV.Location = new System.Drawing.Point(98, 15);
            this.lblPressureV.Name = "lblPressureV";
            this.lblPressureV.Size = new System.Drawing.Size(17, 12);
            this.lblPressureV.TabIndex = 12;
            this.lblPressureV.Text = "-1";
            // 
            // lblPistonLengthT
            // 
            this.lblPistonLengthT.AutoSize = true;
            this.lblPistonLengthT.Location = new System.Drawing.Point(6, 75);
            this.lblPistonLengthT.Name = "lblPistonLengthT";
            this.lblPistonLengthT.Size = new System.Drawing.Size(50, 12);
            this.lblPistonLengthT.TabIndex = 11;
            this.lblPistonLengthT.Text = "　長さ[m]";
            // 
            // lblPistonAreaT
            // 
            this.lblPistonAreaT.AutoSize = true;
            this.lblPistonAreaT.Location = new System.Drawing.Point(6, 55);
            this.lblPistonAreaT.Name = "lblPistonAreaT";
            this.lblPistonAreaT.Size = new System.Drawing.Size(60, 12);
            this.lblPistonAreaT.TabIndex = 10;
            this.lblPistonAreaT.Text = "　面積[m2]";
            // 
            // lblVolumeT
            // 
            this.lblVolumeT.AutoSize = true;
            this.lblVolumeT.Location = new System.Drawing.Point(6, 35);
            this.lblVolumeT.Name = "lblVolumeT";
            this.lblVolumeT.Size = new System.Drawing.Size(52, 12);
            this.lblVolumeT.TabIndex = 9;
            this.lblVolumeT.Text = "体積[m3]";
            // 
            // lblPressureT
            // 
            this.lblPressureT.AutoSize = true;
            this.lblPressureT.Location = new System.Drawing.Point(6, 15);
            this.lblPressureT.Name = "lblPressureT";
            this.lblPressureT.Size = new System.Drawing.Size(60, 12);
            this.lblPressureT.TabIndex = 8;
            this.lblPressureT.Text = "BC圧[kPa]";
            // 
            // frmDebug
            // 
            this.ClientSize = new System.Drawing.Size(384, 461);
            this.Controls.Add(this.tabControl1);
            this.MaximizeBox = false;
            this.Name = "frmDebug";
            this.Text = "AtsPT4";
            this.tabControl1.ResumeLayout(false);
            this.tabMain.ResumeLayout(false);
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            this.ResumeLayout(false);

        }

#endregion

        #region コンストラクタ
        public frmDebug()
        {
            InitializeComponent();
            this.Show();
        }
        #endregion

        public void SetText(LabelKind_Enum kind, string txt)
        {
            switch (kind)
            {
                case LabelKind_Enum.BcPressure:
                    lblPressureV.Text = txt;
                    break;
                case LabelKind_Enum.Volume:
                    lblVolumeV.Text = txt;
                    break;
                case LabelKind_Enum.PistonArea:
                    lblPistonAreaV.Text = txt;
                    break;
                case LabelKind_Enum.PistonLength:
                    lblPistonLengthV.Text = txt;
                    break;
                case LabelKind_Enum.Mol:
                    lblMolV.Text = txt;
                    break;
                case LabelKind_Enum.R:
                    lblRV.Text = txt;
                    break;
                case LabelKind_Enum.T:
                    lblTempratureV.Text = txt;
                    break;
                case LabelKind_Enum.Bane:
                    lblSpringV.Text = txt;
                    break;
                case LabelKind_Enum.BlowArea:
                    lblBlowAreaV.Text = txt;
                    break;
                case LabelKind_Enum.DeltaMol:
                    lblDMolV.Text = txt;
                    break;
                case LabelKind_Enum.BlowSpeed:
                    lblBlowSpeedV.Text = txt;
                    this.Refresh();
                    break;
                default:
                    break;
            }
        }
    }
#endif
    }
