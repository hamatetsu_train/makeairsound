﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
//using System.Threading.Tasks;

using AtsPlugin.Importing;
using AtsPlugin.MotorNoise;
using AtsPlugin.Parametrics;

namespace AtsPlugin
{
    public class clsSound
    {
        /*
        /// <summary>
        /// 設定
        /// </summary>
        private clsConfig pCFG;

        /// <summary>
        /// エンジン動作音クラス
        /// </summary>
        private AtsMotorNoise EngineNoise = null;

        /// <summary>
        /// シリンダー圧力[kPa]
        /// </summary>
        private double pBC;
        private double bBC;
        /// <summary>
        /// シリンダー長さ[m]
        /// </summary>
        private double pL;
        private double bL;
        /// <summary>
        /// シリンダー内気体モル数[mol]
        /// </summary>
        private double pMol;
        private double bMol;

        /// <summary>
        /// 共鳴周波数[Hz]
        /// </summary>
        private double pFreq;

        /// <summary>
        /// 0℃[K]
        /// </summary>
        private const double Temperature = 273.15;
        /// <summary>
        /// 気体の比熱/平均分子量
        /// </summary>
        private const double SpecificHeat = 48.61;
        /// <summary>
        /// モル気体定数[J/mol.K]
        /// </summary>
        private const double AirRatio = 8.314;
        /// <summary>
        /// 標準気圧[kPa]
        /// </summary>
        private const double NormalAirPressure = 101.325;
        */

        /// <summary>
        /// 【初期化】
        /// </summary>
        public void Initialize()
        {
            /*
            if (System.IO.File.Exists(this.pCFG.MotorNoiseTxtPath) == true && System.IO.File.Exists(this.pCFG.SoundTxtPath) == true)
            {
                // 音声インスタンス
                EngineNoise = AtsMotorNoiseImporter.LoadAsset(this.pCFG.MotorNoiseTxtPath, this.pCFG.SoundTxtPath, this.pCFG.EngineSectionName);
            }

            // 初期値設定
            this.pBC = NormalAirPressure;
            this.pL = this.pCFG.SylinderLength;
            this.pMol = (this.pBC * this.pL * this.pCFG.PistonArea) / (AirRatio * Temperature);

            this.bBC = this.pBC;
            this.bL = this.pL;
            this.bMol = this.pMol;
            */
        }

        public void Elapse(double BcPressure, int deltaT)
        {
            /*
            // - 計算
            if (BcPressure <= 0)
            {
                // 緩解状態：初期値
                this.pBC = NormalAirPressure;
                this.pL = this.pCFG.SylinderLength;
                this.pMol = (this.pBC * this.pL * this.pCFG.PistonArea) / (AirRatio * Temperature);
            }
            else if(BcPressure == this.pBC + NormalAirPressure)
            {
                // 変化なし
                this.pFreq = 0;
            }
            else
            {
                this.pBC = NormalAirPressure + BcPressure;
                // 1. F = k * dL
                // 2. F = dP * S
                // => dL = (S / k) * dP
                this.pL = this.pCFG.SylinderLength + (this.pCFG.PistonArea / this.pCFG.Bane) * BcPressure;
                // 1. V = S * (L * dL)
                // 2. PV = nRT
                // => n = P * S * (L * dL) / RT
                this.pMol = (this.pBC * this.pCFG.PistonArea * this.pL) / (AirRatio * Temperature);
            }

            if (this.pBC < this.bBC)
            {
                // 排気された
                // - 閉管共鳴
                //this.pFreq = 4.0 * this.pL / 1.0;
                // - カルマン渦
                this.pFreq = 0.2 * (this.pMol - this.bMol) * AirRatio * Temperature / (NormalAirPressure * Math.PI * Math.Pow(this.pCFG.BlowRadius, 3));

                double dN = (this.pMol - this.bMol) / (double)deltaT * 1000.0;
            }
            else
            {
                // 給気 or 変化なし
                this.pFreq = 0;
            }

            // - 音声
            if (this.EngineNoise != null)
            {
                this.EngineNoise.DirectionMixtureRatio = 1.0f;
                // ピッチ
                this.EngineNoise.Position = (float)this.pFreq;
                // ボリューム
                this.EngineNoise.Volume = 1.0f;
                //
                this.EngineNoise.Update();
            }
            */
        }
    }
}
